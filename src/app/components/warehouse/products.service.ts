import {Injectable} from '@angular/core';
import {Query, gql} from 'apollo-angular';

export interface Product {
  key: string;
  pkey: string;
  brand: string;
  name: string;
  ean: string;
  category: string;
  quantity: number;
  buying_price: number;
  selling_price: number;
}
export interface Response {
  products: Product[];
}

@Injectable()
export class AllProductsGQL extends Query<Response> {
  document = gql`
    query allProducts {
        products {
            key,
            pkey,
            brand,
            name,
            ean,
            category,
            quantity,
            buying_price,
            selling_price
        }
    }
  `;
}