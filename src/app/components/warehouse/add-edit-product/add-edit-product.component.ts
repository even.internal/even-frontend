import { ProductFormComponent } from './../product-form/product-form.component';
import { Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';


@Component({
  selector: 'app-add-edit-product',
  templateUrl: './add-edit-product.component.html',
  styleUrls: ['./add-edit-product.component.sass'],
  providers:[MatDialog]
})
export class AddEditProductComponent implements OnInit {
  constructor(public dialog: MatDialog) { 
  }

  ngOnInit(): void {
  }
  reLoad(){
    //DA CAMBIARE ASSOLUTAMENTE!!!!!!!!!!
    //DA CAMBIARE ASSOLUTAMENTE!!!!!!!!!!
    window.location.reload();
  }
  openNewDialog() {
    this.dialog.open(ProductFormComponent,{
      data:{
        isAddMode:true,
        isDeleteMode: false
      }
  }).afterClosed().subscribe(res => {
      this.reLoad()
    }); ; 
}

}
