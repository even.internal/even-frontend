import { Product, AllProductsGQL } from './../products.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { ProductsTableDataSource } from './products-table-datasource';
import {MatDialog} from '@angular/material/dialog';
import { map } from 'rxjs/operators';
import { ProductFormComponent } from '../product-form/product-form.component';

@Component({
  selector: 'app-products-table',
  templateUrl: './products-table.component.html',
  styleUrls: ['./products-table.component.css'],
  providers: [AllProductsGQL,MatDialog],
})
export class ProductsTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<Product>;
  dataSource!: ProductsTableDataSource;
  selection = new SelectionModel<Product>(true, []);
  displayedColumns = ['select','brand' ,'name', 'ean', 'quantity', 'category','buying_price','selling_price','actions'];
  data: Product[] = [];
  

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  constructor(private AllProdsGQL: AllProductsGQL, public dialog: MatDialog) {}

  ngOnInit() {
    this.AllProdsGQL.watch()
      .valueChanges.pipe(map((result) => result.data.products))
      .subscribe((result) => {
        result.forEach((val) => this.data.push(Object.assign({}, val)));
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
      });
  }

  ngAfterContentChecked() {
    this.dataSource = new ProductsTableDataSource(this.data);
  }
  reLoad(){    
    //DA CAMBIARE ASSOLUTAMENTE!!!!!!!!!!
    //DA CAMBIARE ASSOLUTAMENTE!!!!!!!!!!
    window.location.reload();
  }
  editProduct(curProd: Product){
    //console.log(curProd)
    this.dialog.open(ProductFormComponent,  {data: {
      curProd: curProd,
      isAddMode: false,
      isDeleteMode: false
    }}).afterClosed().subscribe(res => {this.reLoad()}); 
  }

  deleteProduct(curProd: Product){
    this.dialog.open(ProductFormComponent,  {data: {
      curProd: curProd,
      isAddMode: false,
      isDeleteMode: true
    }}).afterClosed().subscribe(res => {this.reLoad()});  
  }
  
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Product): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.key
    }`;
  }
}
