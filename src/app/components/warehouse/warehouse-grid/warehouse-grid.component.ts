import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-warehouse-grid',
  templateUrl: './warehouse-grid.component.html',
  styleUrls: ['./warehouse-grid.component.css'],
})
export class WarehouseGridComponent {
  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          // { title: 'Aggiungi Prodotto', cols: 1, rows: 1, tag:'add-edit-product' },
          // { title: 'Rimuovi Prodotti', cols: 1, rows: 1 },
          { title: 'Lista Prodotti', cols: 1, rows: 1 , tag:'products-table' },
        ];
      }

      return [
        // { title: 'Aggiungi Prodotto', cols: 1, rows: 1, tag:'add-edit-product' },
        // { title: 'Rimuovi Prodotti', cols: 1, rows: 1 },
        { title: 'Lista Prodotti', cols: 2, rows: 2 , tag:'products-table' },
      ];
    })
  );

  constructor(private breakpointObserver: BreakpointObserver) {}
}
