import { Component, Inject, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidationErrors,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from '../products.service';
import { NewProductGQL } from './../new-product.service';

export interface DialogData {
  isAddMode: Boolean;
  isDeleteMode: Boolean;
  curProd: Product;
}

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.sass'],
  providers: [NewProductGQL],
})
export class ProductFormComponent implements OnInit {
  productForm!: FormGroup;
  eanRegx = /^[0-9]{13}$/;
  integerRegx = /^[0-9]*$/;
  Category = ['Vending', 'OCS'];
  isAddMode!: Boolean;
  isDeleteMode!: Boolean;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<ProductFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private newProdGQL: NewProductGQL
  ) {}

  ngOnInit(): void {
    this.isAddMode = this.data.isAddMode;
    this.isDeleteMode = this.data.isDeleteMode;
    this.productForm! = this.formBuilder.group({
      brand: [null, [Validators.required]],
      name: [null, [Validators.required]],
      ean: [null, [Validators.required, Validators.pattern(this.eanRegx)]],
      quantity: [
        null,
        [
          Validators.required,
          Validators.min(0),
          Validators.pattern(this.integerRegx),
        ],
      ],
      category: [null, Validators.required],
      buying_price: [null, [Validators.required, Validators.min(0)]],
      selling_price: [null, [Validators.required, Validators.min(0)]],
    });
    if (!this.isAddMode) {
      this.productForm.patchValue({
        brand: this.data.curProd.brand,
        name: this.data.curProd.name,
        ean: this.data.curProd.ean,
        quantity: this.data.curProd.quantity,
        category: this.data.curProd.category,
        buying_price: this.data.curProd.buying_price,
        selling_price: this.data.curProd.selling_price,
      });
    }
  }

  cleanCategory(str: string) {
    return str.slice(str.indexOf(' ') + 1);
  }
  changeCategory(e: any) {
    this.productForm.get('category')!.setValue(e.target.value, {
      onlySelf: false,
    });
  }
  submit() {
    if (!this.productForm.valid) {
      console.log('form not valid');
    }
    if (this.isAddMode) {
      this.newProdGQL
        .mutate({
          pkey: null,
          brand: this.productForm.value.brand,
          name: this.productForm.value.name,
          ean: this.productForm.value.ean,
          quantity: this.productForm.value.quantity,
          category: this.cleanCategory(this.productForm.value.category),
          buying_price: this.productForm.value.buying_price,
          selling_price: this.productForm.value.selling_price,
          isdeleted: false,
          transaction_dt: Date.now(),
        })
        .subscribe((result) => console.log(result));
    } else {
      if (this.data.curProd.pkey == null) {
        this.data.curProd.pkey = this.data.curProd.key;
      }
      if (this.isDeleteMode) {
        this.newProdGQL
          .mutate({
            pkey: this.data.curProd.pkey,
            brand: this.data.curProd.brand,
            name: this.data.curProd.name,
            ean: this.data.curProd.ean,
            quantity: this.data.curProd.quantity,
            category: this.cleanCategory(this.data.curProd.category),
            buying_price: this.data.curProd.buying_price,
            selling_price: this.data.curProd.selling_price,
            isdeleted: true,
            transaction_dt: Date.now(),
          })
          .subscribe((result) => console.log(result));
      } else if (!this.isAddMode && !this.isDeleteMode) {
        this.newProdGQL
          .mutate({
            pkey: this.data.curProd.pkey,
            brand: this.productForm.value.brand,
            name: this.productForm.value.name,
            ean: this.productForm.value.ean,
            quantity: this.productForm.value.quantity,
            category: this.cleanCategory(this.productForm.value.category),
            buying_price: this.productForm.value.buying_price,
            selling_price: this.productForm.value.selling_price,
            isdeleted: false,
            transaction_dt: Date.now(),
          })
          .subscribe((result) => console.log(result));
      }
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
