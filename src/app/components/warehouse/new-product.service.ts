import {Injectable} from '@angular/core';
import {Mutation, gql} from 'apollo-angular';

@Injectable()
export class NewProductGQL extends Mutation {
  document = gql`
    mutation newProduct($pkey: String, $brand: String!, $name: String!, $ean: String!, $category: String!, $quantity: Int!, $buying_price: Float!, $selling_price: Float!, $isdeleted: Boolean!, $transaction_dt: Int!) {
      createProduct(input: {
        pkey: $pkey,
        brand: $brand,
        name: $name,
        ean: $ean,
        quantity: $quantity,
        category: $category,
        buying_price: $buying_price,
        selling_price: $selling_price,
        isdeleted: $isdeleted,
        transaction_dt:$transaction_dt
      }) {
        key
      }
    }
  `;
}
